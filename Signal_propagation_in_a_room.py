import random
import matplotlib.pyplot as plt
import matplotlib.patches as patches

RoomWidth = 100
RoomHeight = 100
TxLocation = (20,25)
RxLocation = (90,80)
No_Refection = 2

fig2 = plt.figure()
fig = plt.figure()

ax = fig.add_subplot(111,aspect='equal')
ax.set_xlabel('X')
ax.set_ylabel('Y')
#ax.set_xlim([-No_Refection*RoomWidth,(No_Refection+1)*RoomWidth])
#ax.set_ylim([-No_Refection*RoomHeight,(No_Refection+1)*RoomHeight])
ax.set_xlim([-No_Refection*RoomWidth,(No_Refection+1)*RoomWidth])
ax.set_ylim([-No_Refection*RoomHeight,(No_Refection+1)*RoomHeight])
colorTable = ['white','yellow','pink','green','lightblue','skyblue','magenta']
#colorTable = ['white','yellow','pink','gray','gray','gray','gray']

ax2 = fig2.add_subplot(111,aspect='equal')
ax2.set_xlabel('X')
ax2.set_ylabel('Y')
#ax.set_xlim([-No_Refection*RoomWidth,(No_Refection+1)*RoomWidth])
#ax.set_ylim([-No_Refection*RoomHeight,(No_Refection+1)*RoomHeight])
ax2.set_xlim([0,RoomWidth-1])
ax2.set_ylim([0,RoomHeight-1])
# Receivers in mirrored rooms

TxLoc = {}
RxLoc = {}

L = []

for row in range(-No_Refection,No_Refection+1):
    for col in range(-No_Refection,No_Refection+1):
        # Rooms
        ButtomRoom_id = ax.add_patch(
            patches.Rectangle(
            (row*RoomWidth,col*RoomHeight),   # (x,y)
            RoomWidth,          # width
            RoomHeight,         # height
            facecolor = colorTable[(abs(row)+abs(col))%len(colorTable)],alpha=0.3,
            edgecolor='black'))
        # Tx
        if col%2==0:
            xloc = col*RoomWidth + TxLocation[0]
        else:
            xloc = col*RoomWidth + RoomWidth - TxLocation[0]
        if row%2==0:
            yloc = row*RoomHeight + TxLocation[1]
        else:
            yloc = row*RoomHeight + RoomHeight - TxLocation[1]
        Tx_id = ax.add_patch(
            patches.Circle(
            (xloc,yloc),   # (x,y)
            2,          # radius
            color = 'red',alpha=0.6))
        ax2.add_patch(
            patches.Circle(
            (xloc,yloc),   # (x,y)
            2,          # radius
            color = 'red',alpha=0.6))
        TxLoc[(col,row)] = (xloc,yloc)
        # Rx
        if col%2==0:
            xloc = col*RoomWidth + RxLocation[0]
        else:
            xloc = col*RoomWidth + RoomWidth - RxLocation[0]
        if row%2==0:
            yloc = row*RoomHeight + RxLocation[1]
        else:
            yloc = row*RoomHeight + RoomHeight - RxLocation[1]
        Rx_id  = ax.add_patch(
            patches.Circle(
            (xloc,yloc),   # (x,y)
            2,          # radius
            color = 'blue',alpha=0.6))
        ax2.add_patch(
            patches.Circle(
            (xloc,yloc),   # (x,y)
            2,          # radius
            color = 'blue',alpha=0.6))
        RxLoc[(col,row)] = (xloc,yloc)
        if (abs(row) + abs(col)) <= No_Refection:
            line = [[TxLocation[0],RxLoc[(col,row)][0]],[TxLocation[1],RxLoc[(col,row)][1]]]
            L.append(line)
# Rays

def checkleft(line):
    m = (line[1][-1]-line[1][-2])/(line[0][-1]-line[0][-2])
    yx0 = m*(0-line[0][-2]) + line[1][-2]
    if yx0 >= 0 and yx0 < RoomWidth and line[0][-1] < 0:
        col = line[0][-1]//RoomWidth
        row = line[1][-1]//RoomHeight
        line[0][-1] = 0
        line[1][-1] = yx0

        line[0].append(RxLoc[(-col-1,row)][0])
        line[1].append(RxLoc[(-col-1,row)][1])
        return True
    else:
        return False

def checkright(line):
    m = (line[1][-1]-line[1][-2])/(line[0][-1]-line[0][-2])
    yxmax = m*(RoomWidth-1-line[0][-2]) + line[1][-2]
    if yxmax >= 0 and yxmax < RoomWidth and line[0][-1] >= RoomWidth:
        col = line[0][-1]//RoomWidth
        row = line[1][-1]//RoomHeight
        line[0][-1] = RoomWidth-1
        line[1][-1] = yxmax

        line[0].append(RxLoc[(-col+1,row)][0])
        line[1].append(RxLoc[(-col+1,row)][1])
        return True
    else:
        return False

def checkdown(line):
    m = (line[0][-1]-line[0][-2])/(line[1][-1]-line[1][-2])
    xy0 = m*(0-line[1][-2]) + line[0][-2]
    if xy0 >= 0 and xy0 < RoomWidth and line[1][-1] < 0:
        col = line[0][-1]//RoomWidth
        row = line[1][-1]//RoomHeight
        line[0][-1] = xy0
        line[1][-1] = 0

        line[0].append(RxLoc[(col,-row-1)][0])
        line[1].append(RxLoc[(col,-row-1)][1])
        return True
    else:
        return False

def checkup(line):
    m = (line[0][-1]-line[0][-2])/(line[1][-1]-line[1][-2])
    xymax = m*(RoomHeight-1-line[1][-2]) + line[0][-2]
    if xymax >= 0 and xymax < RoomHeight and line[1][-1] >= RoomHeight:
        col = line[0][-1]//RoomWidth
        row = line[1][-1]//RoomHeight
        line[0][-1] = xymax
        line[1][-1] = RoomHeight-1

        line[0].append(RxLoc[(col,-row+1)][0])
        line[1].append(RxLoc[(col,-row+1)][1])
        return True
    else:
        return False

for line in L:

    while line[0][-1] < 0 or line[0][-1] >= RoomWidth or line[1][-1] < 0 or line[1][-1] >= RoomHeight:
        if checkleft(line):
            pass
        elif checkright(line):
            pass
        elif checkdown(line):
            pass
        elif checkup(line):
            pass


    ax.plot(line[0],line[1])
    ax2.plot(line[0],line[1])

plt.show()
